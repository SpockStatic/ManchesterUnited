import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Felulet extends JFrame {


    private JLabel againstLabel;
    private JLabel homeLabel;
    private JLabel datelabel;
    private JLabel scoredLabel;
    private JLabel recivedLabel;
    private JLabel win;
    private JLabel luse;
    private JLabel draw;
    private JLabel points;
    private JLabel scoRec;
    private JLabel Pts;


    private TextField againstText;
    //private TextField homeText;
    private JComboBox homeCombo;
    private TextField dateText;
    private JComboBox scoredCombo;
    private JComboBox recivedCombo;
    private JButton addGame;
    private JButton allGamesToTextF;
    private JButton statistic;



    Felulet() {
        super("Mérkőzések");
        setLayout(null);
        String [] idegenben ={"H","I"};
        String [] score ={"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"};

        againstLabel = new JLabel("Ellenfél: ");
        homeLabel = new JLabel("Idegenben: ");
        homeCombo = new JComboBox(idegenben);
        datelabel = new JLabel("Dátum: ");
        scoredLabel = new JLabel("Rugott: ");
        scoredCombo = new JComboBox(score);
        recivedLabel = new JLabel("Kapott: ");
        recivedCombo = new JComboBox(score);
        againstText = new TextField(10);
        dateText = new TextField("0000-00-00");
        win = new JLabel();
        luse = new JLabel();
        draw = new JLabel();
        scoRec = new JLabel();
        Pts = new JLabel();


        againstLabel.setBounds(10,10,50,25);
        add(againstLabel);
        againstText.setBounds(60,10,125,25);
        add(againstText);
        homeLabel.setBounds(220,10,80,25);
        add(homeLabel);
        homeCombo.setBounds(300,10,50,25);
        add(homeCombo);
        datelabel.setBounds(10,90,50,25);
        add(datelabel);
        dateText.setBounds(60,90,125,25);
        add(dateText);
        scoredLabel.setBounds(10,50,50,25);
        add(scoredLabel);
        scoredCombo.setBounds(60,50,50,25);
        add(scoredCombo);
        recivedLabel.setBounds(220,50,80,25);
        add(recivedLabel);
        recivedCombo.setBounds(300,50,50,25);
        add(recivedCombo);
        win.setBounds(10,130,100,25);
        add(win);
        luse.setBounds(10,155,100,25);
        add(luse);
        draw.setBounds(10,180,100,25);
        add(draw);
        scoRec.setBounds(10,205,300,25);
        add(scoRec);
        Pts.setBounds(10,230,100,25);
        add(Pts);

        addGame = new JButton("Hozzáadás");
        addGame.setBounds(220,90,130,30);
        add(addGame);

        allGamesToTextF = new JButton("Összes Mérkőzés");
        allGamesToTextF.setBounds(220,125,130,30);
        add(allGamesToTextF);

        statistic = new JButton("Statisztika");
        statistic.setBounds(220,160,130,30);
        add(statistic);

        /*b6 = new JButton("Minden Mérkőzés");
        b6.setBounds(320,190,150,30);
        add(b6);*/

        addGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String against = againstText.getText();
                String home = homeCombo.getSelectedItem().toString();
                String date = dateText.getText();
                String scored = scoredCombo.getSelectedItem().toString();
                String recived = recivedCombo.getSelectedItem().toString();

                int scoredInt =Integer.parseInt(scored);
                int recivedInt = Integer.parseInt(recived);

                System.out.println(against+"-"+home+"-"+date+"-"+scored+"-"+recived);

                if(!against.isEmpty() && date.indexOf("-")==4 && date.lastIndexOf("-")==7){

                    DB db = new DB();
                    db.addGames(against,home,date,scoredInt,recivedInt);

                    JOptionPane.showMessageDialog(null,against+ " elleni mérkőzés el lett mentve.");

                }else {
                    JOptionPane.showMessageDialog(null,"Az összes mezőt ki kell tőlteni vagy \nrossz a dátum formátuma pl.: 2022-12-25");
                }


            }
        });

        allGamesToTextF.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AllGamesToTextField allGamesToTextField = new AllGamesToTextField();
                allGamesToTextField.setVisible(true);
                allGamesToTextField.setSize(372,447);
                allGamesToTextField.setResizable(false);
                allGamesToTextField.setLocationRelativeTo(null);
            }
        });

        statistic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DB db = new DB();


                win.setText("Győzelem: "+db.win());
                luse.setText("Vereség: "+db.luse());
                draw.setText("Döntetlen: "+db.draw());
                scoRec.setText("Rugott gól:  "+db.scored()+"  Kapott gól:  "+db.recived()+"  Gólkülömbség:  "+db.scoreDifference());
                Pts.setText("Gyűjtött pontok: "+db.Points());
            }
        });


    }


}
