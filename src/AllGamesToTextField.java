import javax.swing.*;
import java.util.ArrayList;

public class AllGamesToTextField extends JFrame {
    DB db = new DB();

private JScrollPane scrollPane;

    AllGamesToTextField() {

        super("Összes mérkőzés");
        String all="";
        JTextArea jTextArea = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(jTextArea);

        setLayout(null);

        scrollPane.setBounds(4, 4, 350, 400);
        add(scrollPane);

        jTextArea.append(allGames());

        }

        public String allGames() {

            ArrayList<Games> games = db.getAllGames();

            String strGame = "";

            for (Games g : games) {

                strGame += (g.getEllenfel() + "\n" + "  " + g.getIdegenben() + "   " + "   " + g.getDátum() + "   " + g.getRugott_gól() + " - " + g.getKapott_gól() + "\n");


            }

            return strGame;
        }
}











