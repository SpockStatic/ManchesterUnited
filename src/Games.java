public class Games {

    private String Ellenfél;
    private String Idegenben;
    private String Dátum;
    private int Rugott_gól;
    private int Kapott_gól;


    public Games(String Ellenfél, String Idegenben, String Dátum, int Rugott_gól, int Kapott_gól) {
        this.Ellenfél = Ellenfél;
        this.Idegenben = Idegenben;
        this.Dátum = Dátum;
        this.Rugott_gól = Rugott_gól;
        this.Kapott_gól = Kapott_gól;

    }

    public String getEllenfel() {
        return Ellenfél;
    }

    public void setEllenfel(String Ellenfél) {
        this.Ellenfél = Ellenfél;
    }

    public String getIdegenben() {
        return Idegenben;
    }

    public void setIdegenben(String Idegenben) {
        this.Idegenben = Idegenben;
    }

    public String getDátum() {
        return Dátum;
    }


    public void setDátum(String dátum) {
        this.Dátum = dátum;
    }

    public int getRugott_gól() {
        return Rugott_gól;
    }

    public void setRugott_gól(int rugott_gól) {
        this.Rugott_gól = rugott_gól;
    }

    public int getKapott_gól() {
        return Kapott_gól;
    }

    public void setKapott_gól(int kapott_gól) {
        this.Kapott_gól = kapott_gól;
    }


}