import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DB {
    final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    final String URL = "jdbc:mysql://localhost:3306/manchesterutd?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    final String USERNAME ="root";
    final String PASSWORD ="";

    //Létrehozzuk a kapcsolatot (hidat)
    Connection conn = null;
    Statement createStatement = null;
    DatabaseMetaData dbmd = null;

    //Megpróbáljuk életre kelteni
    public DB() {

        try {
            conn = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            System.out.println("A híd létrejött");
        } catch (SQLException ex) {
            System.out.println("Valami baj van a connection (híd) létrehozásakor.");
            System.out.println("" + ex);
        }

        //Ha életre kelt, csinálunk egy megpakolható teherautót
        if (conn != null) {
            try {
                createStatement = conn.createStatement();
            } catch (SQLException ex) {
                System.out.println("Valami baj van van a createStatament (teherautó) létrehozásakor.");
                System.out.println("" + ex);
            }
        }

        //Megnézzük, hogy üres-e az adatbázis? Megnézzük, létezik-e az adott adattábla.
        try {
            dbmd = conn.getMetaData();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a DatabaseMetaData (adatbázis leírása) létrehozásakor..");
            System.out.println("" + ex);
        }

        try {
            ResultSet rs = dbmd.getTables(null, "APP", "USERS", null);
            if (!rs.next()) {
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van az adattáblák létrehozásakor.");
            System.out.println("" + ex);
        }
    }

    /** Mérkőzések hozzáadása az adatbázishoz*/
    public void addGames(String against, String home, String date, int scored, int recived) {
        try {
            String sql = "insert into season2223 values (?,?,?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, against);
            preparedStatement.setString(2, home);
            preparedStatement.setString(3, date);
            preparedStatement.setInt(4, scored);
            preparedStatement.setInt(5, recived);
            preparedStatement.setInt(6,0);

            preparedStatement.execute();
        } catch (SQLException ex) {
            System.out.println("Valami baj van a user hozzáadásakor");
            System.out.println("" + ex);
        }
    }

    /* Kiírja konzolra az összes mérkőzést*/
    public void showAllGames() {
        String all = "";
        String sql = "select * from season2223";
        try {
            ResultSet rs = createStatement.executeQuery(sql);
            while (rs.next()) {
                String Ellenfél = rs.getString("Ellenfél");
                String Idegenben = rs.getString("Idegenben");
                String Dátum = rs.getString("Dátum");
                int Rugott_gól = rs.getInt("RugottGól");
                int Kapott_gól = rs.getInt("KapottGól");
                System.out.println(Ellenfél + " | " + Idegenben+" | "+  Dátum + " | " + Rugott_gól+" | "+Kapott_gól);

            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a userek kiolvasásakor");
            System.out.println("" + ex);
        }
    }

    public void showMeta(){
        String sql = "select * from season2223";
        ResultSet rs = null;
        ResultSetMetaData rsmd = null;
        try {
            rs = createStatement.executeQuery(sql);
            rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            for (int x = 1; x <= columnCount; x++){
                System.out.print(rsmd.getColumnName(x) + " | ");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /** Az összes mérkőzés ArrayList-be gyűjtése*/
    public ArrayList<Games> getAllGames(){
        String sql = "select * from season2223";
        ArrayList<Games> game = null;
        try {
            ResultSet rs = createStatement.executeQuery(sql);
            game = new ArrayList<>();

            while (rs.next()){
                Games actualUser = new Games(rs.getString("Ellenfél"),rs.getString("Idegenben"), rs.getString("Dátum"),rs.getInt("RugottGól"),rs.getInt("KapottGól"));
                game.add(actualUser);
            }
        } catch (SQLException ex) {
            System.out.println("Valami baj van a Games kiolvasásakor");
            System.out.println(""+ex);
        }

        return game;
    }

    public int win(){
        String sql ="SELECT count(*) FROM season2223 where RugottGól > KapottGól ";

        int countWin=0;

        try{
            ResultSet rs = createStatement.executeQuery(sql);
                while (rs.next()){
                    countWin = rs.getInt("count(*)");
                }

        }catch (SQLException e){
            System.out.println("Valami baj van a win kiolvasásakor");
        }

        return countWin;

    }

    public int luse(){
        String sql ="SELECT count(*) FROM season2223 where RugottGól < KapottGól ";

        int countLuse=0;

        try{
            ResultSet rs = createStatement.executeQuery(sql);
            while (rs.next()){
                countLuse = rs.getInt("count(*)");
            }

        }catch (SQLException e){
            System.out.println("Valami baj van a luse kiolvasásakor");
        }

        return countLuse;

    }

    public int draw(){
        String sql ="SELECT count(*) FROM season2223 where RugottGól = KapottGól ";

        int countDraw=0;

        try{
            ResultSet rs = createStatement.executeQuery(sql);
            while (rs.next()){
                countDraw = rs.getInt("count(*)");
            }

        }catch (SQLException e){
            System.out.println("Valami baj van a draw kiolvasásakor");
        }

        return countDraw;

    }

    public int scored(){

        String sql ="SELECT Sum(RugottGól) FROM season2223";

        int scored=0;

        try{

            ResultSet rs = createStatement.executeQuery(sql);

            while (rs.next()){
                scored = rs.getInt("Sum(RugottGól)");
            }

        }catch (SQLException e){
            System.out.println("Valami baj van a rugott gólok kiolvasásakor");
        }
        return scored;
    }

    public int recived(){

        String sql ="SELECT Sum(KapottGól) FROM season2223";

        int rescived=0;

        try{

            ResultSet rs = createStatement.executeQuery(sql);

            while (rs.next()){
                rescived = rs.getInt("Sum(KapottGól)");
            }

        }catch (SQLException e){
            System.out.println("Valami baj van a rugott gólok kiolvasásakor");
        }
        return rescived;
    }

    public int scoreDifference(){

       return scored()-recived();
    }

    public int Points(){
        int pts=0;
        int win=0;
        int draw=0;

        win = win() * 3;
        draw = draw();

        pts = win+draw;

        return pts;
    }


}